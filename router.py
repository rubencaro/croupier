#-*- coding:utf-8 -*-

import re
import imp
import sys
import traceback
import web
from lib.helpers import *
import controllers
if __debug__:  # only for debugging/testing
    import tests
    from tests.controller import TestController

"""

A generic router is always handy, and match requested routes against
a dict gives speed/security.

First we get all routes we can guess from our controllers' code.

Any modules inside 'controllers' package can define classes
named with a 'Controller' suffix, such as 'MyIndexController'. Those
classes will define the first level of the routes dict. We take their name,
remove the 'Controller' suffix, and underscore the remainings.
So 'MyIndexController' becomes 'my_index'.

Then, for each controller found for the first level, we look for methods
named with an '_action' suffix. Those methods will be the second level of
the routes dict. Just taking their name and removing the '_action' suffix.
So 'fancy_action' becomes 'fancy'.

The hard work is already done. The routes dict is already filled
with the exact words that will match the given urls.

When a requests arrives, the given url is matched against "/(\w*)/*(\w*)",
and captures are passed as 'controller' and 'action' parameters.
Then the router simply looks in the routes dict for
a 'routes[controller][action]' element. If it exists, an instance of
the corresponding controller class is created, then called
the matching action method. If it doesn't exist, then 404 is returned.

An example:
    (in module controllers.fancyindexmodule)
        class MyIndexController:
            def fancy_action:
                return web.ok()
            def another_action:
                return web.ok()
            def not_in_routes:
                print 'not an action method'
        class NotInRoutes:
            def lost_action:
                print 'not in a controller'


    Then the routes dict will be something like:
        routes = {
            'my_index' = {
                'fancy' = <function fancy_action>,
                'another' = <function another_action>
            }
        }

    So when a request arrives with an url like '/my_index/another',
    then the action executed is MyIndexController#another_action. Any
    combination of controller/action not in the routes dict gets a 404 response.

"""
routes = {}

class Router:

    def POST(self,controller,action):
        return GET(controller,action)

    def GET(self,controller,action):
        global routes
        try:
            if not routes:
                self.guess_routes()

            if controller == '':
                controller = 'index'
            if action == '':
                action = 'index'

            c = routes.get(controller,None)
            if not c:
                if __debug__:  # only for debugging/testing
                    print spit(routes)
                return web.notfound()

            a = routes[controller].get(action,None)
            if not a:
                if __debug__:  # only for debugging/testing
                    print spit(routes)
                return web.notfound()
        except:
            if __debug__:  # only for debugging/testing
                print yellow(traceback.format_exc())
            return web.notfound()

        try:
            if __debug__:  # only for debugging/testing
                print 'Handling request with: ' + routes[controller]['controller_class'].__name__ + '#' + a.__name__
                tests.calls.append(web.ctx.fullpath)

            instance = routes[controller]['controller_class']()
            return a(instance)
        except:
            print yellow(traceback.format_exc())
            spit(routes)
            return web.internalerror()


    def guess_routes(self):
        print "Guessing routes..."
        global routes
        routes = {}
        controller_pattern = re.compile('^.+Controller$')
        action_pattern = re.compile('^.+_action$')
        for modname in controllers.__all__:
            (file, pathname, description) = imp.find_module(modname,['controllers'])
            loaded_mod = imp.load_module('controllers.' + modname, file, pathname, description)
            controller_names = [ a for a in loaded_mod.__dict__.keys() if controller_pattern.match(a) ]
            for controller_name in controller_names:
                controller_route = underscore( controller_name.replace('Controller','') )
                routes[controller_route] = {}
                controller = loaded_mod.__dict__.get(controller_name)
                routes[controller_route] = {}
                routes[controller_route]['controller_class'] = controller
                action_names = [ a for a in controller.__dict__.keys() if action_pattern.match(a) ]
                for action_name in action_names:
                    action = controller.__dict__.get(action_name)
                    action_route = action_name.replace('_action','')
                    routes[controller_route][action_route] = action
        if __debug__:
            routes['test'] = {}
            routes['test']['controller_class'] = TestController
            action_names = [ a for a in TestController.__dict__.keys() if action_pattern.match(a) ]
            for action_name in action_names:
                action = TestController.__dict__.get(action_name)
                action_route = action_name.replace('_action','')
                routes['test'][action_route] = action
