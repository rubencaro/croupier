#!/usr/bin/env python
#-*- coding:utf-8 -*-

"""

Yea, testing the test machinery !

"""
import sys,os
sys.path.append( os.path.abspath( os.getcwd() ) )
from tests.helper import *
import json

def test_calls():
    b = SimpleBrowser()

    b.get('http://localhost:3000/test/get_calls')
    data = b.check_json()
    b.check(data == ['/test/get_calls'], 'data: ' + purple(data))

    b.get('http://localhost:3000/test/get_calls')
    data = b.check_json()
    b.check(data == ['/test/get_calls','/test/get_calls'], 'data: ' + purple(data))

    b.get('http://localhost:3000/test/clear_calls')
    b.check_ok()

    b.get('http://localhost:3000/test/get_calls')
    data = b.check_json()
    b.check(data == ['/test/get_calls'], 'data: ' + purple(data))

if __name__ == "__main__":
    run_tests( globals().values() )
