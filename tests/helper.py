#!/usr/bin/env python
#-*- coding:utf-8 -*-

"""

Actual test runner is here.

"""

import sys
import traceback
import types
import re
import json
from subprocess import Popen
from time import sleep
import urllib

def spit(obj, msg = ''):
    print purple('\n' + str(msg) + ': ' + str(obj) + '\n')

def red(msg):
    return '\033[1;31m' + str(msg) + '\033[0m'

def green(msg):
    return '\033[1;32m' + str(msg) + '\033[0m'

def brown(msg):
    return '\033[0;33m' + str(msg) + '\033[0m'

def yellow(msg):
    return '\033[1;33m' + str(msg) + '\033[0m'

def cyan(msg):
    return '\033[0;36m' + str(msg) + '\033[0m'

def purple(msg):
    return '\033[1;35m' + str(msg) + '\033[0m'

def run_tests(mod_dict):
    print brown("Starting server...")
    app_process = Popen(['python','main.py','3000'])
    sleep(2)

    tested = 0
    failed = 0
    try:
        for f in get_function_list(mod_dict):
            try:
                tested += 1
                print brown('Running ' + f.__name__ + '...')
                f()
            except Exception:
                failed += 1
                print red( f.__name__ + ' failure!' )
                print yellow(traceback.format_exc())
    finally:
        print brown("Terminating server...")
        app_process.terminate()
        print "\n\nTested: %d, Failed: %d\n" % (tested,failed)
        if failed > 0:
            print red("\n--- Failure ! ---\n")
            exit(1)
        print green("\n--- Success ! ---\n")

def get_function_list(func_list):
    pattern = re.compile('^test_.+')
    return [ a for a in func_list if ( hasattr(a,'__name__') and
            pattern.match(a.__name__) and
            isinstance(a, types.FunctionType) )]

"""

in test helpers, to be inherited by a Browser

"""
class TestHelper:

    def check(self,exp,msg = ''):
        if callable(msg):
            msg = msg()
        assert exp, cyan('\n' + str(msg))

    def check_status(self,status):
        self.check( str(self.reply_data.get('status','')) == str(status),
                ("status should be '%s' but:\n" % str(status)) + purple(str(self.reply_data)) )

    def check_ok(self):
        self.check_status(200)

    # first url, before redirections
    def check_original_url(self,url):
        self.check( url == self.reply_data.get('original_url',''),
                 ("original_url should be '%s' but:\n" % url) + purple(str(self.reply_data)) )

    # last url, after all redirections
    def check_url(self,url):
        self.check( url == self.reply_data.get('url',''),
                 ("url should be '%s' but:\n" % url) + purple(str(self.reply_data)) )

    def check_content_type(self, content_type):
        self.check( self.reply_data.get('content-type','') == content_type,
                 ("content-type should be '%s' but:\n" % content_type) + purple(str(self.reply_data)) )

    def check_html(self):
        self.check_ok()
        self.check_content_type('text/html')

    def check_json(self):
        self.check_ok()
        self.check_content_type('application/json')
        return json.loads( self.reply_data.get('body','') )

    def check_has(self,pattern):
        self.check( re.findall( pattern, self.reply_data.get('body',''), re.IGNORECASE ),
                ("No occurrences of '%s' inside:\n" % pattern) + purple(self.reply_data.get('body',''))  )

"""

WebkitBrowser override, headless js testing

Example:
    b = Browser('zope.testbrowser')
    b.get('http://localhost:3000')
    b.check_html()

"""

from webscraping import webkit
from PyQt4.QtNetwork import QNetworkRequest

class Browser(webkit.WebkitBrowser, TestHelper):

    def finished(self, reply):
        self.reply_data = {}
        for header in reply.rawHeaderList():
            self.reply_data[str(header).lower()] = str(reply.rawHeader(header))
        self.reply_data['status'] = reply.attribute(QNetworkRequest.HttpStatusCodeAttribute).toString()
        self.reply_data['reason'] = reply.attribute(QNetworkRequest.HttpReasonPhraseAttribute).toString()
        self.reply_data['redirection'] = reply.attribute(QNetworkRequest.RedirectionTargetAttribute).toString()
        self.reply_data['encrypted'] = reply.attribute(QNetworkRequest.ConnectionEncryptedAttribute).toString()
        self.reply_data['cache_load'] = reply.attribute(QNetworkRequest.CacheLoadControlAttribute).toString()
        self.reply_data['cache_save'] = reply.attribute(QNetworkRequest.CacheSaveControlAttribute).toString()
        self.reply_data['from_cache'] = reply.attribute(QNetworkRequest.SourceIsFromCacheAttribute).toString()
        self.reply_data['url'] = self.history().currentItem().url().toString()
        self.reply_data['original_url'] = self.history().currentItem().originalUrl().toString()

    def get(self, url):
        self.reply_data['body'] = super(Browser,self).get(url)
        return self.reply_data['body']


"""

Browser-like struct for simple urllib calls, reuse check helpers

Example:
    b = SimpleBrowser()
    b.get('http://localhost:3000')
    b.check_html()

"""
class SimpleBrowser(TestHelper):

    def get(self,url):
        reply = urllib.urlopen(url)
        self.reply_data = {}
        for header in reply.headers.dict.keys():
            self.reply_data[str(header)] = str(reply.headers.dict[header])
        self.reply_data['status'] = reply.getcode()
        self.reply_data['url'] = reply.geturl()
        self.reply_data['original_url'] = url
        self.reply_data['body'] = reply.read()
        return self.reply_data['body']
