#!/usr/bin/env python
#-*- coding:utf-8 -*-

"""

Controller meant to be imported from the app in testing mode.

The app, when in testing mode, should gather some useful data.
Any access to that data can be done from here, and then transferred back
to test code.

"""

import web
import json
import tests

class TestController:

    def get_calls_action(self):
        web.header('Content-Type', 'application/json')
        return json.dumps(tests.calls)

    def clear_calls_action(self):
        tests.calls = []
        return web.ok()
