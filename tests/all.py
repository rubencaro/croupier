#!/usr/bin/env python
#-*- coding:utf-8 -*-

import sys,os
sys.path.append( os.path.abspath( os.getcwd() ) )
import tests
from tests.helper import *
import pkgutil
import re
from importlib import import_module

def get_all_test_functions():
    test_functions = []

    # recorrer la jerarquía de módulos desde aquí
    for loader, module_name, is_pkg in \
                        pkgutil.walk_packages(tests.__path__):
        # recolectar todas las funciones de test
        pattern = re.compile('.+_test$')
        if pattern.match(module_name):
            module = import_module(module_name)
            new_functs = get_function_list( module.__dict__.values() )
            test_functions.extend(new_functs)
    return test_functions

if __name__ == "__main__":
    run_tests( get_all_test_functions() )
