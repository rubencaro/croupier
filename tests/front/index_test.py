#!/usr/bin/env python
#-*- coding:utf-8 -*-

import sys,os
sys.path.append( os.path.abspath( os.getcwd() ) )
from tests.helper import *

def test_index():
    b = SimpleBrowser()

    b.get('http://localhost:3000')
    b.check_html()
    b.check_has( 'id="playboard"' )

    b.get('http://localhost:3000/index')
    b.check_html()
    b.check_has( 'id="playboard"' )

    b.get('http://localhost:3000/index/index')
    b.check_html()
    b.check_has( 'id="playboard"' )

if __name__ == "__main__":
    run_tests( globals().values() )
