#!/usr/bin/env python
#-*- coding:utf-8 -*-

from webscraping import webkit
from PyQt4.QtNetwork import QNetworkRequest

class Browser(webkit.WebkitBrowser):
    def finished(self, reply):
        self.reply_data = {}
        for header in reply.rawHeaderList():
            self.reply_data[str(header)] = str(reply.rawHeader(header))
        self.reply_data['status'] = reply.attribute(QNetworkRequest.HttpStatusCodeAttribute).toString()
        self.reply_data['reason'] = reply.attribute(QNetworkRequest.HttpReasonPhraseAttribute).toString()
        self.reply_data['redirection'] = reply.attribute(QNetworkRequest.RedirectionTargetAttribute).toString()
        self.reply_data['encrypted'] = reply.attribute(QNetworkRequest.ConnectionEncryptedAttribute).toString()
        self.reply_data['cache_load'] = reply.attribute(QNetworkRequest.CacheLoadControlAttribute).toString()
        self.reply_data['cache_save'] = reply.attribute(QNetworkRequest.CacheSaveControlAttribute).toString()
        self.reply_data['from_cache'] = reply.attribute(QNetworkRequest.SourceIsFromCacheAttribute).toString()
