#-*- coding:utf-8 -*-
import re

def spit(obj, msg = ''):
    print purple('\n' + str(msg) + ': ' + str(obj) + '\n')

def red(msg):
    return '\033[1;31m' + str(msg) + '\033[0m'

def green(msg):
    return '\033[1;32m' + str(msg) + '\033[0m'

def brown(msg):
    return '\033[0;33m' + str(msg) + '\033[0m'

def yellow(msg):
    return '\033[1;33m' + str(msg) + '\033[0m'

def cyan(msg):
    return '\033[0;36m' + str(msg) + '\033[0m'

def purple(msg):
    return '\033[1;35m' + str(msg) + '\033[0m'

def underscore(name):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()

def capitalize(name):
    return name.replace('_',' ').title().replace(' ','')   
