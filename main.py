#!/usr/bin/env python
#-*- coding:utf-8 -*-

import web
import web.template
from router import Router

urls = ("/(\w*)/*(\w*)", Router)
render = web.template.render('templates/', base='layout', globals=globals())
app = web.application(urls, globals())

if __name__ == "__main__":
    app.run()
