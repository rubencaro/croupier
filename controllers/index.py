#!/usr/bin/env python
#-*- coding:utf-8 -*-
import web
from main import render

class IndexController:

    def index_action(self):
        web.header('Content-Type', 'text/html')
        return render.index()
